#
# nixpics -- UNIX-style photo-managing utilities
#
# Copyright (C) 7, Evan Battaglia <gtoevan@gmx.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

import pygtk
pygtk.require('2.0')
import gtk

import gc
import time
import threading

def sample_resize(pix):
   return pix.scale_simple(400,300,gtk.gdk.INTERP_BILINEAR)

class Loader(threading.Thread):
    
  def __init__(self,resize_func=sample_resize):
    threading.Thread.__init__(self)
    self.cache = {}
    self.worklist = []
    self.resize_func = resize_func
    self.cachelock = threading.Lock()
    self.worklistlock = threading.Condition()

  def start_load(self,file):
    self.worklistlock.acquire()
    self.worklist.insert(0,file)
    self.worklistlock.notify()
    self.worklistlock.release()

  def get_image_if_loaded(self,file):
    pix = None
    self.cachelock.acquire()
    if self.cache.has_key(file):
      pix = self.cache[file]
    self.cachelock.release()
    return pix

  def free_image(self,file):
    self.cachelock.acquire()
    if self.cache.has_key(file):
      del self.cache[file]
      gc.collect()
    self.cachelock.release()

  def die(self):
    self.worklistlock.acquire()
    self.worklist = []
    self.worklistlock.notify()
    self.worklistlock.release()    

  def run(self):
    while True:
      self.worklistlock.acquire()

      if not self.worklist:
        self.worklistlock.wait()
      if not self.worklist: # killed
        return

      filename = self.worklist.pop()
      self.worklistlock.release()

      print "loading %s" % filename

      # TODO: ERRORS
      bigpix = None
      lilpix = None
      bigpix = gtk.gdk.pixbuf_new_from_file(filename)
#      time.sleep(2)
      print "bixpix %s" % filename

      lilpix = self.resize_func(bigpix)
      del bigpix
      gc.collect()

      print "caching %s" % filename

      self.cachelock.acquire()
      self.cache[filename] = lilpix
      self.cachelock.release()

      print "done caching %s" % filename
