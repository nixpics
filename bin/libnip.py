#!/usr/bin/env python
#
# nixpics -- UNIX-style photo-managing utilities
#
# Copyright (C) 7, Evan Battaglia <gtoevan@gmx.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

import pyexiv2
import datetime
import os

NIP_VERSION = "0.0.1"

NAME_TAG = 'Iptc.Application2.Headline'
TAGS_TAG = 'Iptc.Application2.Keywords'
DESC_TAG = 'Iptc.Application2.Caption'
DATE_TAG = 'Exif.Image.DateTime'
WIDTH_TAG = 'Exif.Photo.PixelXDimension'
HEIGHT_TAG = 'Exif.Photo.PixelYDimension'

# general purpose enum
TYPE_NAME=0
TYPE_DESC=1
TYPE_TAGS=2

class Image(pyexiv2.Image):

  def __init__(self,f):
    pyexiv2.Image.__init__(self,f)
    self.filename = f
    pyexiv2.Image.readMetadata(self)

  def get_name(self):
    if NAME_TAG not in self.iptcKeys():
      return None
    return self[NAME_TAG]

  def get_tags(self,tagdelim=None):
    if TAGS_TAG not in self.iptcKeys():
      return None
    tags = self[TAGS_TAG]
    if tagdelim != None and type(tags) != str:
      return tagdelim.join(tags)
    return tags

  def get_desc(self):
    if DESC_TAG not in self.iptcKeys():
      return None
    return self[DESC_TAG]

  def get_date(self):
    if DATE_TAG not in self.exifKeys():
      return None
    return self[DATE_TAG]

  def set_name(self,name):
    self[NAME_TAG] = name

  def set_tags(self,tags,string_sep=" "):
    if type(tags) == str:
      tags = tags.split(string_sep)
    print tags
    self[TAGS_TAG] = tags

  def set_desc(self,desc):
    self[DESC_TAG] = desc

  def set_date(self,date):
    self[DATE_TAG] = date

  def get_of_type(self,t,tagdelim=None):
    if t == TYPE_NAME:
      return self.get_name()
    if t == TYPE_DESC:
      return self.get_desc()
    if t == TYPE_TAGS:
      return self.get_tags(tagdelim)

  def set_of_type(self,t,x,tagdelim=" "):
    if t == TYPE_NAME:
      return self.set_name(x)
    if t == TYPE_DESC:
      return self.set_desc(x)
    if t == TYPE_TAGS:
      return self.set_tags(x,tagdelim)

  def get_width(self):
    return self[WIDTH_TAG]
  def get_height(self):
    return self[HEIGHT_TAG]

  def write(self):
    self.writeMetadata()

  # todo: different sanitize functions for everything and get rid of space char and excludes and join.
  def strftime_plus(self,s,name_spacechar=None,tags_join=" ",tags_spacechar=None,tags_excludes=[], \
                    desc_spacechar=None, sanitize_func=None):
    """Similar to strftime, replaces, in the string argument, sequences like
%%d, %Y, %m with the year, month and date from the photo. Also replaces the
following non-date-related % sequences:

%n     name, with spaces converted to arg spacechar if given
%t     tags, except those in arg tags_excludes, with tags_spacechar
%s     description, with spaces converted to arg desc_spacechar if given
%P     original path
%F     basename of original filename, minus suffix (no suffix or directory)
%E     extension of original filename, such as ".JPG" or ".jpeg"

Note: you may have to switch "bad" characters from the output of this, since
name, tags, description could have newlines, tabs, or other "bad characters"."""
    import datetime

    # really should only calculate if needed.
#    print "debug %s %s" % (self.get_date(),self.filename)
    date = self.get_date()
    name = self.get_name()

    tags_list = self.get_tags()
    if tags_list is None: tags_list = []
    tags_list = [i for i in tags_list if i not in tags_excludes]

    tags = tags_join.join(tags_list)

    desc = self.get_desc()

    if name is None: name = ""
    if tags is None: tags = ""
    if desc is None: desc = ""

    if name_spacechar is not None:
      name = name.replace(" ",name_spacechar)
    if tags_spacechar is not None:
      tags = tags.replace(" ",tags_spacechar)
    if desc_spacechar is not None:
      desc = desc.replace(" ",desc_spacechar)

    if sanitize_func is not None:
      name = sanitize_func(name)
      tags = sanitize_func(tags)
      desc = sanitize_func(desc)

    s = s.replace("%n",name)
    s = s.replace("%t",tags)
    s = s.replace("%s",desc)
    s = s.replace("%P",os.path.dirname(self.filename))
    s = s.replace("%F",os.path.basename(os.path.splitext(self.filename)[0]))
    s = s.replace("%E",os.path.splitext(self.filename)[1])

    if date:
        s = date.strftime(s)
    return s

    # if no date???


