#!/usr/bin/env python
#
# nixpics -- UNIX-style photo-managing utilities
#
# Copyright (C) 7, Evan Battaglia <gtoevan@gmx.net>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
#

# TODO: SANITIZE BETTER!!!

# TODO: add options, add %i for index

# TODO: lowercase things, pass in to strftime_plus "filters" to do what want
# code should just give things like filter_compose(lowercase,spaces_to_underscore)

# TODO: backport strftime_plus to nip-show

import libnip
import sys, getopt, os, tempfile, datetime

PROGRAM_NAME = "nip-mv"

def usage():
  print """usage: %s <formatstring> <image> [image]...
type %s --help for more options""" % (PROGRAM_NAME,PROGRAM_NAME)

def help():
  print """%s (part of nip %s)
Renames photos according to a formatstring formula. Files are never overwritten; instead,
a "2", "3", "4" etc. is added before the extension to create a new name that doesn't exist.

Usage: %s <formatstring> <image> [image]...
  <formatstring> is a strftime-style format string representing a template
  for each new photo name. In addition to typical strftime values (%%d, %%H, %%M etc.),
  the following are defined:
     %%n     name
     %%t     tags
     %%s     description
     %%i     index of image file on commadn line or chronologically.
             starts from 0 and was enough 0's to make sure dictionary order holds.
     %%P     original path of filename (USE IF MOVING FILES IN DIFFERENT DIRECTORIES!)
     %%F     basename of original filename, minus suffix (no suffix or directory)
     %%E     extension of original filename, such as ".JPG" or ".jpeg"

  -l         don't actually rename; just list each file with its new filename.
             doesn't do full naming resolution: only does it if the file already existed.
  -t         order images chronologically from EXIF timestamp (use with %%i in formatstring)
  -s suffix  add 'suffix' before number when resolving filename conflicts
  -p         like mkdir -p, automatically make directories if necessary.
  -v         verbose
  --help     show this help screen


Example:
  %s -t "%%i-%%n.jpg" *.jpg
renames files 01-bear_catching_fish.jpg, 02-bear_eating_fish.jpg, etc., if
  photos have names "bear catching fish", "bear eating fish", ... and 01, 02
  represent correct order from timestamps.
""" % (PROGRAM_NAME, libnip.NIP_VERSION, PROGRAM_NAME, PROGRAM_NAME)

def sanitize(s):
  # TODO: custom from file (?)
  s = s.replace("/","")
  s = s.replace("\\","")
  return s
  # TODO IMPORTANT! SANITIZE BETTER!!!

def main():
  try:
    opts, args = getopt.gnu_getopt(sys.argv[1:], "ltspv", ["help"])
  except getopt.GetoptError:
    # print help information and exit:
    usage()
    sys.exit(2)

  listonly = False
  orderbytime = False
  suffix_num_prefix = ""
  makenessdirs = False
  verbose = False

  for o, a in opts:
    if o == "-p":
      makenessdirs = True
    if o == "-s":
      suffix_num_prefix = a
    if o == "-l":
      listonly = True
    if o == "-t":
      orderbytime = True
    if o == "-v":
      verbose = True
    if o == "--help":
      help()
      sys.exit()
    # ...

  if len(args) < 2:
    sys.stderr.write("error: not enough args\n")
    usage()
    sys.exit(1)

  format_str = args[0]

  dates_fns_and_new_fns = [] # tuples of (fn,new_fn)
  realpaths_to_indices = {} # table from realpaths to indices.

  index = 0
  for fn in args[1:]:
    realpath = os.path.realpath(fn)
    if realpaths_to_indices.has_key(realpath):
      sys.stderr.write("Ignoring duplicate %s\n", fn)
      continue

    im = libnip.Image(fn)
    new_fn = im.strftime_plus(format_str,name_spacechar="_",tags_spacechar="_",desc_spacechar="_", \
                         tags_join=" ",tags_excludes=[],sanitize_func=sanitize)
    imdate = im.get_date()
    if imdate:
        dates_fns_and_new_fns.append ( [imdate,fn,new_fn] )
    else: # no date -- dummy date
        dates_fns_and_new_fns.append ( [datetime.datetime(1,1,1,0,0,0),fn,new_fn] )

    realpaths_to_indices[realpath] = index
    index += 1

  if orderbytime:
    dates_fns_and_new_fns.sort()
    # fix realpaths to indices now that they've changed
    for i in range(len(dates_fns_and_new_fns)):
      realpath = os.path.realpath(dates_fns_and_new_fns[i][1])
      realpaths_to_indices[realpath] = i


  # %i handling
  index = 0
  l = len(dates_fns_and_new_fns)
  if l < 10: digits = "%01d"
  elif l < 100: digits = "%02d"
  elif l < 1000: digits = "%03d"
  elif l < 10000: digits = "%04d"
  else: digits = "%05d"

  index = -1 # start at 0
  for (date,fn,new_fn) in dates_fns_and_new_fns:
    index += 1

    # override protection: this should be controllable.
    # TODO: if overriding one that we're renaming -- rename old to a temp location first and change args accdlingy
    new_fn = new_fn.replace("%i", (digits % index))

    if os.path.realpath(fn) == os.path.realpath(new_fn): continue

    # NAME RESOLUTION: IF FILE EXISTS
    # 1) if file is in our list of "todo" stuff and we haven't seen it yet,
    #    move it to a temporary location and change the entry.
    # 2) try adding a "2" to it, or whatever number we're up to
    # 3) if that exists, go back to (1)

    suffix_num = 2
    orig_new_fn = new_fn
    while os.path.exists(new_fn) and os.path.realpath(new_fn) != os.path.realpath(fn): # stop if find a new name that doesn't exist or is itself
      new_realpath = os.path.realpath(new_fn)
      if realpaths_to_indices.has_key(new_realpath) and realpaths_to_indices[new_realpath] > index:
        # the existing file will be processed in due time. move it to a temp location for now, and update list of files
        # same extension, same direcoryy, called nip-mv-XXXXXX
        tempfn = tempfile.mkstemp(os.path.splitext(new_fn)[1],"nip-mv-",os.path.dirname(new_realpath))[1]
        if listonly:
          print "%s:%s" % (new_fn, tempfn)
          os.remove(tempfn)
        else:
          if verbose:
            print "%s:%s" % (new_fn, tempfn)
          os.rename (new_fn,tempfn)
        dates_fns_and_new_fns[realpaths_to_indices[new_realpath]][1] = tempfn # update the fn so we can find it.
        break
      (new_name,new_ext) = os.path.splitext(orig_new_fn)
      new_fn = "%s%s%d%s" % (new_name, suffix_num_prefix, suffix_num, new_ext)
      suffix_num += 1

    if os.path.realpath(fn) == os.path.realpath(new_fn): continue



    if listonly:
      print "%s:%s" % (fn, new_fn)
    else:
      if verbose:
        print "%s:%s" % (fn, new_fn)
      os.renames(fn,new_fn) # NOTE: renames makes directories ness. nice, eh?

if __name__ == "__main__":
    main()
